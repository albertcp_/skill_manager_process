cmake_minimum_required(VERSION 2.8.3)
add_definitions(-std=c++11)

set(PROJECT_NAME skill_manager_process)
project(${PROJECT_NAME})


set(SKILL_MANAGER_PROCESS_SOURCE_DIR
  src/source)
  
set(SKILL_MANAGER_PROCESS_INCLUDE_DIR
  src/include)

set(SKILL_MANAGER_PROCESS_SOURCE_FILES
  ${SKILL_MANAGER_PROCESS_SOURCE_DIR}/skill_manager_process.cpp
  ${SKILL_MANAGER_PROCESS_SOURCE_DIR}/skill_manager_process_main.cpp
)
  
set(SKILL_MANAGER_PROCESS_HEADER_FILES
  ${SKILL_MANAGER_PROCESS_INCLUDE_DIR}/skill_manager_process.h  
)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  drone_process
  roscpp
  std_msgs
  droneMsgsROS
  droneModuleROS
  droneModuleInterfaceROS
  lib_pugixml
  geometry_msgs
  angles
)

###################################
## catkin specific configuration ##
###################################
catkin_package(  
  INCLUDE_DIRS ${SKILL_MANAGER_PROCESS_INCLUDE_DIR}
  #LIBRARIES skill_manager_process
  CATKIN_DEPENDS roscpp std_msgs droneMsgsROS drone_process droneModuleROS droneModuleInterfaceROS lib_pugixml angles
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(${catkin_INCLUDE_DIRS})

add_library(skill_manager_process_lib ${SKILL_MANAGER_PROCESS_SOURCE_FILES} ${SKILL_MANAGER_PROCESS_HEADER_FILES})
add_dependencies(skill_manager_process_lib ${catkin_EXPORTED_TARGETS})
target_link_libraries(skill_manager_process_lib ${catkin_LIBRARIES})

add_executable(skill_manager_process ${SKILL_MANAGER_PROCESS_SOURCE_DIR}/skill_manager_process_main.cpp)
add_dependencies(skill_manager_process ${catkin_EXPORTED_TARGETS})
target_link_libraries(skill_manager_process skill_manager_process_lib)
target_link_libraries(skill_manager_process ${catkin_LIBRARIES})
