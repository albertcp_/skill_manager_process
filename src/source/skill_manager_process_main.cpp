#include "../include/skill_manager_process.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, ros::this_node::getName());
  SkillManager skill_manager_process;
  skill_manager_process.setUp();
  try
  {
    skill_manager_process.start();
  } catch(std::exception& exception)
  {
    skill_manager_process.notifyError(skill_manager_process.SafeguardRecoverableError,0,"ownStart()",exception.what());
    skill_manager_process.stop();    // The process will have to be started by a service call from another process
  }

  ros::spin();
  
  return 0;
}
